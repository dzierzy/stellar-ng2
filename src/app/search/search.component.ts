import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {FormGroup, FormBuilder, FormControl} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html'
})
export class SearchComponent implements OnInit {

  searchForm = new FormGroup({
    name: new FormControl()
  });
  dbs: Object = [];
  selected;
  selectedRow;
  query: string;

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      name: ''
    });
  }

  search() {
    console.log('searching...');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log('lets post! name:[' + this.searchForm.value.name + ']');
    let uri = 'http://localhost:3000/constellations';
    if (this.query) {
      uri = uri + '?query=' + this.query;
    }
    this.http.get<any>(uri)
      .subscribe(res => {
        console.log(res);
        this.notify.emit('result-display');
        this.dbs = res;
      });

    console.log('search finished.');

  }

  changeDb(e: MouseEvent, abbr: any, i: any) {
    console.log('in emit. i:' + i);
    this.selectedRow = i;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get<any>('http://localhost:3000/constellations/' + abbr + '/stars'/*,
      {headers}*/)
      /*.subscribe(res => {
        this.selected = res.json();
       /!* if (this.selected != null) {
          console.log('stars set selected');
          // this.selected = this.dbs[0];
          // this.selectedRow = 0;
        }
        // console.log(this.dbs);
        this.notify.emit('result-display');
        console.log('notification sent');*!/
      });*/
      .subscribe(res => {
        this.selected = res;
      });

    console.log('search finished.');
  }
}

